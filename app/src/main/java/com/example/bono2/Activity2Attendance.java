package com.example.bono2;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.example.bono2.resources.ExpLVAdapter;
import com.example.bono2.resources.Constant;
import com.example.bono2.resources.Student;
import com.example.bono2.resources.VolleyCallback;
import com.opencsv.CSVWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Activity2Attendance extends AppCompatActivity {

    private ExpandableListView expLV;
    private ExpLVAdapter adapter;
    private List<String> listCategorias;
    private Map<String, List<Student>> mapChild;

    /*
        REAL
     */
    private List<String> fechas;
    private List<Student> listStudents;
    private Map<String, String> students;
    private Map<String, List<Student>> studentsAbscence;
    private static final String URL_STUDENT_LIST = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/studentsList?pageSize=100";
    private static final String TAG_SET_STUDENT = "STUDENT";
    private static final String TAG_GET_STUDENT_ABSCANCE = "ABSCENCE";
    private static final String TAG_PRUEBA = "PRUEBA";
    private static final String URL = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/";
    private static final String STUDENTS_PATH = "/students/";

    @SuppressLint("SimpleDateFormat")
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("d-M-yyyy");

    private Calendar initCalendar = Calendar.getInstance();
    private Calendar endCalendar = Calendar.getInstance();


    private Button mReturnMainActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2_attendance);


        expLV = (ExpandableListView) findViewById(R.id.expLV);
        listCategorias = new ArrayList<>();
        mapChild = new HashMap<>();
        students = new HashMap<>();
        studentsAbscence = new HashMap<>();
        listStudents = new ArrayList<>();
        fechas = new ArrayList<>();
        this.getStudents();
        saveData();

        mReturnMainActivity = findViewById(R.id.returnMainActivy);
        mReturnMainActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new Intent(view.getContext(), MainActivity.class);
                startActivityForResult(intent2, 0);
            }
        });
    }

    private void setDates() {
        initCalendar.set(2019, 7, 5);
        endCalendar.set(2019, 7, 31);
    }

    private void getStudents() {
        Constant.httpRequest(Request.Method.GET, URL_STUDENT_LIST, null, getApplicationContext(), new VolleyCallback(){

            @Override
            public void onSuccessResponse(JSONObject response) {
                try {
                    getStudentsList(response);
                    setDates();
                } catch (JSONException e) {
                    Log.w(TAG_SET_STUDENT, e.toString());
                }
                Log.w(TAG_GET_STUDENT_ABSCANCE, "VOY A ENTRAR");
                getStudentAbscence();
            }

            @Override
            public void onErrorResponse(VolleyError e) {
                Toast.makeText(getApplicationContext(),"Predetermined information is set", Toast.LENGTH_SHORT).show();
                Log.w(TAG_PRUEBA, "CARGAR DATA PREESTABLECIDA");
                Log.w(TAG_PRUEBA, e.toString());
                setDates();
                setData();
            }
        });
    }

    private void getStudentsList(JSONObject response) throws JSONException {
        JSONArray document = response.getJSONArray("documents");
        for (int i = 0; i < document.length() ;i++){
            JSONObject jsonObject = document.getJSONObject(i);
            JSONObject fields = null;
            try {
                fields = jsonObject.getJSONObject("fields");
            } catch (Exception e){
                Log.w(TAG_SET_STUDENT, e.toString());
            }
            if(fields != null){
                String name = fields.getJSONObject("name").getString("stringValue");
                String id = fields.getJSONObject("code").getString("stringValue");
                listStudents.add(new Student(name,id));
                students.put(id, name);
            }
        }
    }

    private void getStudentAbscence() {
        Log.w(TAG_GET_STUDENT_ABSCANCE, !endCalendar.before(initCalendar) +"" );
        for (Calendar init = initCalendar; !endCalendar.before(initCalendar) ; init.add(Calendar.DATE, 1)){
            if(init.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY || init.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY || init.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY){
                final String fecha = SIMPLE_DATE_FORMAT.format(init.getTime());
                fechas.add(fecha);
                Log.w(TAG_GET_STUDENT_ABSCANCE, fecha);
                Set<String> studentsID = students.keySet();
                for (final String studentID: studentsID) {
                    String url = URL + fecha + STUDENTS_PATH + studentID;
                    Log.w(TAG_GET_STUDENT_ABSCANCE, url);
                    Log.w(TAG_GET_STUDENT_ABSCANCE, fecha + " " + studentID);
                    Constant.httpRequest(Request.Method.GET, URL_STUDENT_LIST, null, getApplicationContext(),  new VolleyCallback() {

                        @Override
                        public void onSuccessResponse(JSONObject response) {}

                        @Override
                        public void onErrorResponse(VolleyError e) {
                            Log.w(TAG_GET_STUDENT_ABSCANCE, e.toString());
                            Log.w(TAG_GET_STUDENT_ABSCANCE, "NO ESTA INSCRITO");
                            Student student = new Student(students.get(studentID),studentID);
                            List<Student> students = new LinkedList<>();
                            students.add(student);
                            studentsAbscence.put(fecha, students);
                        }
                    });
                }
            }
        }
        try {
            //Ponemos a "Dormir" el programa durante los ms que queremos
            Thread.sleep(5*1000);
        } catch (Exception e) {
            System.out.println(e);
        }
        adapter = new ExpLVAdapter(fechas, studentsAbscence, getApplicationContext());
        expLV.setAdapter(adapter);
    }


    private void setData() {

        Student studentA = new Student("Oliver Amaya", "201212651");
        Student studentB = new Student("Diego Sanabria ", "201217484 ");
        Student studentC = new Student("Juan Nicolas Galvis", "201313973");
        Student studentD = new Student("Sergio Velasquez", "201315851");

        List<Student> students = new ArrayList<>();
        students.add(studentA);
        students.add(studentB);
        students.add(studentC);
        students.add(studentD);

        listStudents.add(studentA);
        listStudents.add(studentB);
        listStudents.add(studentC);
        listStudents.add(studentD);

        Log.w(TAG_PRUEBA, "LOL");
        Log.w(TAG_PRUEBA, SIMPLE_DATE_FORMAT.format(initCalendar.getTime()));
        Log.w(TAG_PRUEBA, !endCalendar.before(initCalendar) + "");

        for (Calendar init = initCalendar; !endCalendar.before(initCalendar) ; init.add(Calendar.DATE, 1)){
            if(init.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY ||
                    init.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY ||
                    init.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY){
                listCategorias.add(SIMPLE_DATE_FORMAT.format(init.getTime()));
                mapChild.put(SIMPLE_DATE_FORMAT.format(init.getTime()), students);
            }
        }

        Log.w(TAG_PRUEBA, listCategorias.size() + "");
        Log.w(TAG_PRUEBA, mapChild.size() + "");
        adapter = new ExpLVAdapter(listCategorias, mapChild, getApplicationContext());
        expLV.setAdapter(adapter);
    }

    private void saveData() {
        ImageButton fab = findViewById(R.id.fab);
        final String csv = (getApplicationContext().getExternalFilesDir("storage") + "/StudentAttendance.csv");
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CSVWriter writer = null;
                try {
                    writer = new CSVWriter(new FileWriter(csv));

                    List<String[]> data = new ArrayList<String[]>();
                    data.add(new String[]{"Fecha","Name", "Code"});

                    if( !studentsAbscence.isEmpty()){
                        for (String key: fechas){
                            List<Student> students = studentsAbscence.get(key);
                            for (Student student: students){
                                Log.w(TAG_PRUEBA, student.getName() + "/" + student.getId());
                                data.add(new String[]{key, student.getName(), student.getId()});
                            }
                        }
                    } else {
                        for (String key: listCategorias){
                            List<Student> students = mapChild.get(key);
                            for (Student student: students){
                                Log.w(TAG_PRUEBA, student.getName() + "/" + student.getId());
                                data.add(new String[]{key, student.getName(), student.getId()});
                            }
                        }
                    }


                    writer.writeAll(data); // data is adding to csv

                    writer.close();

                    Toast.makeText(getApplicationContext(),"Your File is save in " + csv, Toast.LENGTH_SHORT).show();
                    Log.w(TAG_PRUEBA, csv);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }
}
