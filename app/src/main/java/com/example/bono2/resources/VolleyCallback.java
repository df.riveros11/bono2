package com.example.bono2.resources;

import com.android.volley.VolleyError;

import org.json.JSONObject;

public interface VolleyCallback {

    void onSuccessResponse(JSONObject response);

    void onErrorResponse(VolleyError e);
}
