package com.example.bono2.resources;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

public class Constant {


    public static void httpRequest(int method, String url, JSONObject jsonValue, Context context, final VolleyCallback callback) {

        MySingletonRequestQueue.getInstance(context).getRequestQueue();

        JsonObjectRequest response = new JsonObjectRequest(method, url, jsonValue, new Response.Listener <JSONObject> () {

            @Override
            public void onResponse(JSONObject Response) {
                callback.onSuccessResponse(Response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                callback.onErrorResponse(e);
            }
        });
        MySingletonRequestQueue.getInstance(context).addToRequestQueue(response);
    }

    public static void showMessage(String msg, Context context){
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

}
