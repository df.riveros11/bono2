package com.example.bono2;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.example.bono2.resources.Constant;
import com.example.bono2.resources.VolleyCallback;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    /**
     * Variables FINAL
     */
    @SuppressLint("SimpleDateFormat")
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("d-M-yyyy");
    /**
     private static final String URL_TEST = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/5-7-2019/students/201212651";
     */
    private static final String URL = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/";
    private static final String STUDENTS_PATH = "/students";
    private static final String ID_STUDENTS = "/201327471";

    private static final String TAG = "BLUETOOTH";
    private static final String TAG_DATE = "FormatDate";
    private static final String TAG_PRUEBA = "PRUEBA";

    private static final int ENABLE_BT = 0;
    private static final int DISCOVERY_BT = 1;

    /**
     * Botones, texto, que van ha estar en la interfaz del app movil
     */
    private TextView mStatusBlueMAC, mAssistanceMov;
    private ImageView mBlueTv;
    private Button mOnBtn, mOffBtn, mDiscoverBtn, mTryAgain, mAttendance;

    private BluetoothAdapter mBlueAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeBTI();
         /*
          Obtiene la MAC del dispositivo
         */
        this.getMAC();
        /*
          Obtiene si el bluetooth esta disponible
         */
        this.getBluetooth();
        /*
          Pinta la imagen dependiendo de si el bluetooth esta prendido
         */
        this.paintBluetooth();
        /*
            Obtiene si estas inscrito o no en firestore
         */
        this.getAttendance();
        /*
          Prende el bluetooth
         */
        this.getBluetoothOn();
        /*
          Apaga el bluetooth
         */
        this.getBluetoothOff();
        /*
          Hace que el dispositivo sea visible
         */
        this.getDiscoveryOn();
        /*
            Busca denuevo en la base de datos
         */
        mTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAttendance();
            }
        });

        mAttendance.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent (view.getContext(), Activity2Attendance.class);
                    startActivityForResult(intent, 0);
                }catch (Exception e){
                    Log.w(TAG_PRUEBA, e.toString());
                    Constant.showMessage("CORRECTING",getApplicationContext());
                }

            }
        });
    }

    private void initializeBTI() {
        mStatusBlueMAC = findViewById(R.id.statusBluetoothMAC);
        mAssistanceMov = findViewById(R.id.statusAssistanceMov);
        mBlueTv = findViewById(R.id.bluetoothTv);
        mOnBtn = findViewById(R.id.onBtn);
        mOffBtn = findViewById(R.id.offBtn);
        mDiscoverBtn = findViewById(R.id.discoverableBtn);
        mTryAgain = findViewById(R.id.tryagain);
        mAttendance = findViewById(R.id.attendance);
    }

    private void getDiscoveryOn() {
        mDiscoverBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!mBlueAdapter.isDiscovering()){
                    Constant.showMessage("Making Your Device Discoverable", getApplicationContext());
                    Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                    startActivityForResult(intent, DISCOVERY_BT);
                    mBlueTv.setImageResource(R.drawable.ic_action_bluetooth);
                }else{
                    Constant.showMessage("Bluetooth is already Discoverable", getApplicationContext());
                }
            }
        });
    }

    private void getBluetoothOff() {
        mOffBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mBlueAdapter.isEnabled()){
                    mBlueAdapter.disable();
                    Constant.showMessage("Turning Bluetooth Off", getApplicationContext());
                    mBlueTv.setImageResource(R.drawable.ic_action_off);
                }else{
                    Constant.showMessage("Bluetooth is already off", getApplicationContext());
                }
            }
        });
    }

    private void getBluetoothOn() {
        mOnBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!mBlueAdapter.isEnabled()){
                    Constant.showMessage("Turning on Bluetooth", getApplicationContext());
                    Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(intent, ENABLE_BT);
                }else{
                    Constant.showMessage("Bluetooth is already on", getApplicationContext());
                }
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void getAttendance() {
    /*
      Crea el formato de la fecha del dia
     */
        String dateFormat = getDate();

        /*
          Genera la URL de la base de datos
         */
        String urlCompleted = URL + dateFormat + STUDENTS_PATH + ID_STUDENTS;
        Log.w(TAG, urlCompleted);

        /*
          Busca el ID de estudiante con la fecha establecida
         */

        Constant.httpRequest(Request.Method.GET, urlCompleted, null, this,  new VolleyCallback(){

            @Override
            public void onSuccessResponse(JSONObject response) {
                Log.w(TAG, "FOUND");
                Log.w(TAG, response.toString());
                mAssistanceMov.setText("You are registered");
                mTryAgain.setText("You are registered");
            }

            @Override
            public void onErrorResponse(VolleyError e) {
                Log.w(TAG, "NO ESTA INSCRITO");
                Log.w(TAG, e.toString());
                mAssistanceMov.setError(e.toString());
                mAssistanceMov.setText("Not registered");
                mTryAgain.setText("Tell to Mario");
            }
        });
    }

    private String getDate() {
        String dateFormat = "";
        try{
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, -1);
            dateFormat = SIMPLE_DATE_FORMAT.format(calendar.getTime());
            Log.w(TAG_DATE, dateFormat);
        }catch(Exception e){
            Log.w(TAG_DATE, e.toString());
        }
        return dateFormat;
    }

    private void paintBluetooth() {
        if(mBlueAdapter.isEnabled()){
            mBlueTv.setImageResource(R.drawable.ic_action_bluetooth);
        }else{
            mBlueTv.setImageResource(R.drawable.ic_action_off);
        }
    }

    @SuppressLint("SetTextI18n")
    private void getBluetooth() {
        mBlueAdapter = BluetoothAdapter.getDefaultAdapter();
        if(mBlueAdapter == null){
            mBlueTv.setImageResource(R.drawable.ic_action_warning);
        }
    }

    @SuppressLint("SetTextI18n")
    private void getMAC() {
        String macAddress = android.provider.Settings.Secure.getString(this.getContentResolver(), "bluetooth_address");
        if(macAddress != null){
            mStatusBlueMAC.setText(macAddress);
        }else{
            mStatusBlueMAC.setText("It is not possible to find the Bluetooth MAC");
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                mBlueTv.setImageResource(R.drawable.ic_action_bluetooth);
                Constant.showMessage("Bluetooth is on", getApplicationContext());
            } else {
                Constant.showMessage("couldn't on bluetooth", getApplicationContext());
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}
